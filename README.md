Group Member: 	

1.) A. Bayusuto	--> Search/Friend page

2.) Bagus Surya Baskoro --> Status page

3.) Prudence Querida --> History page

4.) Vincentius Aditya Sundjaja --> Login and Profile page

Pipeline Status: [![pipeline status](https://gitlab.com/prettyprettywebsite/prettyprettywebsite2/badges/master/pipeline.svg)](https://gitlab.com/prettyprettywebsite/prettyprettywebsite2/commits/master)

Code Coverage Status: [![coverage report](https://gitlab.com/prettyprettywebsite/prettyprettywebsite2/badges/master/coverage.svg)](https://gitlab.com/prettyprettywebsite/prettyprettywebsite2/commits/master)

Herokuapp Link:	 https://prettyprettywebsite2.herokuapp.com/
