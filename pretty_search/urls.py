from django.conf.urls import url
from pretty_profile.views import index

urlpatterns = [
    url(r'^$', index, name='index'),
]
