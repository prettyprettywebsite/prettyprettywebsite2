from __future__ import unicode_literals

from django.test import TestCase
from django.test import Client

from django.urls import resolve
from .views import *

# Create your tests here.
class PrettyProfileUnitTest(TestCase):
    def test_pretty_search_url_is_exist(self):
         response = Client().get('/pretty_search')
         self.assertEqual(response.status_code, 301) #301 for redirect
