from django.db import models

# Create your models here.
class Expertise(models.Model):
    java = 'java'
    c = 'c'
    python = 'python'
    PPW = 'ppw'
    expertise_choices = (
        (java, 'Java'),
        (c, 'C#'),
        (python, 'Python'),
        (PPW, 'PPW'),
        )
    expertise = models.CharField(
        max_length=20,
        choices=expertise_choices,
	    )
    beginner = 'beginner'
    intermediate = 'intermediate'
    advanced = 'advanced'
    expert = 'expert'
    legend = 'legend'
    level_choices = (
        (beginner, 'Beginner'),
        (intermediate, 'Intermediate'),
        (advanced, 'Advanced'),
        (expert, 'Expert'),
        (legend, 'Legend'),
        )
    level = models.CharField(
        max_length=20,
        choices=level_choices,
        )

    class Meta:
        ordering = ('expertise',)

    def __str__(self): # pragma: no cover
        return self.expertise + self.level

class Friend(models.Model):
    npm = models.CharField(max_length=10)
    name = models.CharField(max_length=400)
    year = models.CharField(max_length=4)
    email = models.EmailField(max_length = 100, null = True)

    expertise = models.ManyToManyField(Expertise)

    class Meta:
        ordering = ('npm',)

    def __str__(self): # pragma: no cover
        return self.name
