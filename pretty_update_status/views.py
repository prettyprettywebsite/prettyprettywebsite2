from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Status_Form
from pretty_auth.models import Status
from pretty_profile.utils import *
# Create your views here.
response = {}
def index(request):
    response['author'] = "Team PrettyPrettyWebsite"
    status = Status.objects.all()
    response['status'] = status
    html = 'pretty_update_status.html'
    response['status_form'] = Status_Form
    return render(request, html, response)

def status_update(request):       #pragma no cover
    form = Status_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        kode_identitas = get_data_user(request, 'kode_identitas')
        user = User.objects.get(npm = kode_identitas)
        response['username'] = get_data_user(request, 'user_login')
        response['message'] = request.POST['message']
        status = Status(message=response['message'], user=user, username=response['username'])
        status.save()
        return HttpResponseRedirect('/pretty_profile/')
    else:
        return HttpResponseRedirect('/pretty_profile/')
