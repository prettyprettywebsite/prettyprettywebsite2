from django import forms

class Status_Form(forms.Form):
    error_messages = {
        'required': 'Please fill this input',
    }
    description_attrs = {
        'type': 'text',
        'cols': 75,
        'rows': 4,
        'class': 'status-form-textarea',
        'placeholder':'How are you today?'
    }

    message = forms.CharField(widget=forms.Textarea(attrs=description_attrs), required=True, label="")
