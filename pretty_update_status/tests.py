from __future__ import unicode_literals

from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *
from pretty_auth.models import Status

# Create your tests here.
class PrettyUpdateStatusUnitTest(TestCase):
    def test_pretty_update_status_url_is_exist(self):
         response = Client().get('/pretty_update_status/')
         self.assertEqual(response.status_code, 200) 

    def test_pretty_update_status_using_index_func(self):
         found = resolve('/pretty_update_status/')
         self.assertEqual(found.func, index)
