from django.apps import AppConfig


class PrettyUpdateStatusConfig(AppConfig):
    name = 'pretty_update_status'
