from pretty_auth.models import User

def check_user_in_database(request, npm): # pragma: no cover
    is_exist = False
    kode_identitas = get_data_user(request, 'kode_identitas')
    count_user = User.objects.filter(npm=kode_identitas).count()
    if count_user > 0 :
        is_exist = True

    return is_exist

def check_user_connected_linkedin(request, npm): # pragma: no cover
    connected = False
    kode_identitas = get_data_user(request, 'kode_identitas')
    user = User.objects.get(npm=kode_identitas)

    if user.name == None or "None":
        connected = False
    else:
        connected = True

    return connected

def get_data_user(request, tipe): # pragma: no cover
    data = None
    if tipe == "user_login" and 'user_login' in request.session:
        data = request.session['user_login']
    elif tipe == "kode_identitas" and 'kode_identitas' in request.session:
        data = request.session['kode_identitas']

    return data

def create_new_user(request): # pragma: no cover
    name = get_data_user(request, 'user_login')
    kode_identitas = get_data_user(request, 'kode_identitas')

    user = User() #only get npm from sso auth
    user.npm = kode_identitas
    user.save()

    return user
