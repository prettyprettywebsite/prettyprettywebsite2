# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import messages
from django.shortcuts import render
from django.http import *
from django.http import QueryDict
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from pretty_auth.views import authenticate
from .utils import *
from pretty_auth.models import User, Status, Expertise
from pretty_search.models import Friend
from pretty_search import views
from django.urls import reverse
from pretty_update_status.forms import Status_Form
from pretty_history.api_riwayat import get_riwayat

response = {}
def index(request): # pragma: no cover
    if not authenticate(request):
        return HttpResponseRedirect(reverse('pretty-auth:index'))
    else:
        response['is_login'] = True
        kode_identitas = get_data_user(request, 'kode_identitas')
        user_exist = check_user_in_database(request, kode_identitas)
        if (user_exist):
            user = User.objects.get(npm = kode_identitas)
        else:
            user = create_new_user(request)
        response['user'] = user
        if Status.objects.filter(user=user).count() > 0:
            latest_status = Status.objects.filter(user=user).latest('created_date')
            response['latest_status'] = latest_status
            status_count = Status.objects.filter(user=user).count()
            response['status_count'] = status_count
        else:
            response['latest_status'] = "You haven't post anything"
            response['status_count'] = 0
        html = 'pretty_auth/session/home.html'
        expertise = Expertise.objects.filter(user=user)
        response['expertise'] = expertise
        response['menuSelected'] = 'profile'
        response['author'] = "Team PrettyPrettyWebsite"

        if user.name == None or "None":
            response['userName'] = get_data_user(request, 'user_login')
            messages.success(request, "Please update your profile")
        else:
            response['userName'] = user.name

        #--------pretty_update_status-------#
        status = Status.objects.filter(user=user)
        response['status'] = status
        response['status_form'] = Status_Form

        #-----------pretty_search-----------#
        friend_list = Friend.objects.all()
        response['friend_list'] = friend_list

        #-----------pretty_history-----------#
        response['riwayats'] = get_riwayat()

        return render(request, html, response)

def edit_profile(request): # pragma: no cover
    if not authenticate(request):
        return HttpResponseRedirect(reverse('pretty-auth:index'))
    else:
        kode_identitas = get_data_user(request, 'kode_identitas')
        user = User.objects.get(npm = kode_identitas)
        if Status.objects.filter(user=user).count() > 0:
            latest_status = Status.objects.filter(user=user).latest('created_date')
            response['latest_status'] = latest_status
            status_count = Status.objects.filter(user=user).count()
            response['status_count'] = status_count
        response['user'] = user
        skill = Expertise.objects.filter(user=user)
        response['skill'] = skill
        response['connected'] = check_user_connected_linkedin(request, kode_identitas)
        html = 'pretty_profile/edit_profile.html'
        return render(request, html, response)

@csrf_exempt
def update_profile(request): # pragma: no cover
    if request.method == 'POST':
        kode_identitas = get_data_user(request, 'kode_identitas')
        user = User.objects.get(npm = kode_identitas)
        fullname = request.POST['full_name']
        url_linkedin = request.POST['url_linkedin']
        picture_url = request.POST['picture_url']
        email = request.POST['email']
        headline = request.POST['headline']
        user.name = fullname
        user.url_linkedin = url_linkedin
        user.photo = picture_url
        user.email = email
        user.headline = headline
        user.save()
        return HttpResponseRedirect(reverse('pretty-profile:edit-profile'))

@csrf_exempt
def edit_profile_local(request): # pragma: no cover
    if request.method == 'POST':
        request.POST = request.POST.copy()
        show = request.POST['show_score']
        expertise = request.POST.getlist('skill')
        level = request.POST.getlist('level')
        kode_identitas = get_data_user(request, 'kode_identitas')
        user = User.objects.get(npm = kode_identitas)
        counter = 0;
        for a in expertise:
            if (Expertise.objects.filter(user=user,expertise=a)):
                expertiseObj = Expertise.objects.get(user=user,expertise=a)
                expertiseObj.expertise = a
                expertiseObj.level = level[counter]
            else:
                expertiseObj = Expertise.objects.create(user=user, expertise=a,level=level[counter])
            expertiseObj.save()
            counter += 1
        user.show_score = show
        user.save()
    return HttpResponseRedirect(reverse('pretty-profile:edit-profile'))

def delete_skill(request,skillname):# pragma: no cover
    print('=====>delete')
    if request.method == 'GET':
        kode_identitas = get_data_user(request, 'kode_identitas')
        user = User.objects.get(npm = kode_identitas)
        Expertise.objects.get(user=user,expertise = skillname).delete()
        return HttpResponseRedirect(reverse('pretty-profile:edit-profile-local'))
