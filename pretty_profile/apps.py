from django.apps import AppConfig


class PrettyProfileConfig(AppConfig):
    name = 'pretty_profile'
