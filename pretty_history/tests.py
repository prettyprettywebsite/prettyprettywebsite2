# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase
from django.test import Client
from django.http import HttpRequest
from django.urls import resolve
from django.contrib.auth.models import *
from .views import *

# Create your tests here.
class PrettyHistoryUnitTest(TestCase):
    def test_pretty_profile_url_is_exist(self):
         response = Client().get('/pretty_history/')
         self.assertEqual(response.status_code, 200)

    def test_pretty_history_using_index_func(self):
         found = resolve('/pretty_history/')
         self.assertEqual(found.func, index)
