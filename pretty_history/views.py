from django.shortcuts import render
from .api_riwayat import get_riwayat

# Create your views here.
response = {}
def index(request):
    response['riwayats'] = get_riwayat()
    html = 'pretty_history/history.html'
    return render(request, html, response)
