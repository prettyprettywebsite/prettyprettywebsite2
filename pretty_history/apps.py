from django.apps import AppConfig


class PrettyHistoryConfig(AppConfig):
    name = 'pretty_history'
