# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages

response = {}

def index(request): # pragma: no cover
    response['author'] = "Team PrettyPrettyWebsite"
    html = "pretty_auth/auth.html"
    response['is_login'] = False
    return render(request, html, response)

def login(request): # pragma: no cover
    print ("#==> masuk login")
    if 'user_login' in request.session:
        response['is_login'] = True
        return HttpResponseRedirect(reverse('pretty-profile:index'))
    else:
        response['is_login'] = False
        html = 'pretty_auth/session/login.html'
        return render(request, html, response)

def authenticate(request):
	return ('user_login' in request.session.keys())
