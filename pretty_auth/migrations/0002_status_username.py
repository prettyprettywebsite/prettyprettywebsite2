# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-12-29 09:08
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pretty_auth', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='status',
            name='username',
            field=models.CharField(max_length=100, null=True),
        ),
    ]
