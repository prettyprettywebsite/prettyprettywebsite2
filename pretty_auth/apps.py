from django.apps import AppConfig


class PrettyAuthConfig(AppConfig):
    name = 'pretty_auth'
