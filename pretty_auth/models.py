from django.db import models

# Create your models here.
class User(models.Model):
	name = models.CharField(max_length=100,null=True)
	npm = models.CharField(max_length=20)
	url_linkedin = models.URLField(null=True)
	photo = models.URLField(null=True)
	email = models.EmailField(max_length=200,null=True)
	show_score = models.BooleanField(default=True)
	headline = models.CharField(max_length=300,null=True)
	created_at = models.DateTimeField(auto_now_add = True)
	updated_at = models.DateTimeField(auto_now = True)

class Expertise(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	java = 'java'
	c = 'c'
	python = 'python'
	PPW = 'ppw'
	expertise_choices = (
		(java, 'Java'),
		(c, 'C#'),
		(python, 'Python'),
		(PPW, 'PPW'),
	)
	expertise = models.CharField(
		max_length=2,
		choices=expertise_choices,
		)
	beginner = 'beginner'
	intermediate = 'intermediate'
	advanced = 'advanced'
	expert = 'expert'
	legend = 'legend'
	level_choices = (
		(beginner, 'Beginner'),
		(intermediate, 'Intermediate'),
		(advanced, 'Advanced'),
		(expert, 'Expert'),
		(legend, 'Legend'),
		)
	level = models.CharField(
		max_length=2,
		choices=level_choices,
		)
	created_at = models.DateTimeField(auto_now_add = True)
	updated_at = models.DateTimeField(auto_now = True)


class Status(models.Model):
	username = models.CharField(max_length=100,null=True)
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	message = models.CharField(max_length=128)
	created_date = models.DateTimeField(auto_now_add = True)
	updated_at = models.DateTimeField(auto_now = True)
