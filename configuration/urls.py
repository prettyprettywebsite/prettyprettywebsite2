"""prettyprettywebsite2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic import RedirectView
import pretty_auth.urls as pretty_auth
import pretty_profile.urls as pretty_profile
import pretty_search.urls as pretty_search
import pretty_update_status.urls as pretty_update_status
import pretty_history.urls as pretty_history

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^pretty_auth/', include(pretty_auth, namespace='pretty-auth')),
    url(r'^pretty_profile/', include(pretty_profile, namespace='pretty-profile')),
    url(r'^pretty_search/', include(pretty_search, namespace='pretty-search')),
    url(r'^pretty_update_status/', include(pretty_update_status, namespace='pretty-update-status')),
    url(r'^pretty_history/', include(pretty_history, namespace= 'pretty-history')),
    url(r'^$', RedirectView.as_view(url='/pretty_auth/', permanent=True), name='index')
]
